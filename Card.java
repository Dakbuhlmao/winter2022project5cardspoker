public class Card {
	private Suite suite;
	private Value value;
	
	
	public Card(Suite suite, Value value){
		
		this.suite = suite;
		this.value = value;
		
	}
	

	public int getValue(){
		return value.getValue();
	}
	
	public String printValue() {
		return value.printValue();
	}


	public String getSuite(){
		return suite.getSuite();
	}

	public int getNumSuite(){
		return suite.getNumSuite();
	}
	
	public String toString(){
		String output = "";
		output += printValue() + " of " + getSuite() + " ";
		return output;
	}
	
}