public class Combination {
  private final int[] valueFreq = new int[13];
  private final int[] suiteFreq = new int[4];
  private DynamicCardArray playerHand; // the player's hand
  private HandType theScore; // the player's score
  public Combination(DynamicCardArray h) {
    playerHand = h;

    for (Card c : playerHand.getCardArray()) {
        if (c == null)
            break;
        valueFreq[c.getValue()]++;
        suiteFreq[c.getNumSuite()]++;
    }
    theScore = Score();
  }
  
  public HandType getScore() {
    return theScore;
  }
  public int getScoreNum(){
      return theScore.getVal();
  }
  public String getScoreString(){
        // this.theScore = Score();
      return this.theScore.getValString();
  }

  private HandType Score() {
    if (checkRoyalFlush()) {
        return HandType.ROYAL_FLUSH;
    } // Royal Flush
    if (checkStraightFlush()) {
        return HandType.STRAIGHT_FLUSH;
    } // Straight Flush
    if (checkMult(4)) {
        return HandType.FOUR_OF_A_KIND;
    } // Four of a Kind
    if (checkFullHouse()) {
        return HandType.FULL_HOUSE;
    } // Full House
    if (checkFlush()) {
        return HandType.FLUSH;
    } // Flush
    if (checkStraight()) {
        return HandType.STRAIGHT;
    } // Straight
    if (checkMult(3)) {
        return HandType.THREE_OF_A_KIND;
    } // Three of a Kind
    if (checkTwoPair()) {
        return HandType.TWO_PAIR;
    } // Two Pairs
    if (checkMult(2)) {
        return HandType.PAIR;
    } // One Pair
    return HandType.HIGH_CARD; // High Card
}
  private boolean checkFlush() {
    for (int i : suiteFreq) {
        if (i == 5) { // if all suits the same
            return true;
        }
    } // otherwise impossible to be a flush
    return false;
  }
  //Có thể cái này gây lỗi
  private boolean checkStraight() {
    int count = 0; // to count the number of successive ranks
    for (int i = 0; i < valueFreq.length - 1; i++) {
        if (valueFreq[i] == valueFreq[i + 1]) {
            // this count will only be five if straight
            count++;
        } else {
            return false;
        }
    }
    return count == 5;
}
  private boolean checkRoyalFlush() {
    // a royal flush is both a flush and straight
    if (checkFlush() && checkStraight()) {
        // and contains ace as top card
        if (playerHand.getTopValue() == 13) {
            return true;
        }
    } // otherwise impossible to be a RF
    return false;
  }
  private boolean checkStraightFlush() {
    return checkFlush() && checkStraight();
  } 
  private boolean checkMult(int i) {
    for (int j : valueFreq) {
        if (j == i) {
            return true;
        }
    } 
    return false;
  }   
  private boolean checkFullHouse() {
    return checkMult(3) && checkMult(2);
  }
  private boolean checkTwoPair() {
    int count = 0; // for counting pairs
    for (int i : valueFreq) {
        if (i == 2) {
            count++;
        }
    }
    return count == 2;
  }
  public void seth(DynamicCardArray h){
    this.playerHand = h;
  }
}
