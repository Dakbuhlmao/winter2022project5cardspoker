  import java.util.*;

	public class Deck { 
		private DynamicCardArray deck;

		public Deck(){
			this.deck = new DynamicCardArray();
			populate();
		}

	private void populate(){
		for(Suite s : Suite.values()){
			for(Value v : Value.values()){
				Card card = new Card(s,v);
				this.deck.addCardToBottom(card);
			}
		}
	}


	public void shuffle(){
		Random rando = new Random();
		for(int i = 0; i < 100; i++) {
			int rdNumber = rando.nextInt(52);
			Card tempoStorage = deck.getCard(1); // vi tri so 1
			deck.set(1,deck.getCard(rdNumber)); // set doi vi tri so 1 voi rdNumber
			deck.set(rdNumber,tempoStorage); // doi vi tri rdNumber voi so 1
		}
	}
	public Card getCardFromTop(){
		return deck.getCard(0);
	}
	public void removeTopCard(){
		deck.removeCard(0);
	}

	public DynamicCardArray getDeck(){
		return this.deck;
	}

	public String toString(){
		String output = deck.showAllCards();
		return output;
	}
}