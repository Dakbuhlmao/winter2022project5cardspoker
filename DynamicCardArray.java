public class DynamicCardArray {
	private Card[] values;
	private int next;
	
	//constructor
	public DynamicCardArray() {
		this.values = new Card[10000];
		this.next = 0;
	}
	
	//add card to the bottom
	public void addCardToBottom(Card s) {
		this.values[next] = s;
		this.next++;
	}
	
	//remove a card from a given index
	public void removeCard(int position) {
	//this thing doesnt need OOB check bc the getCard check for invalid input already
		for (int j = position; j < this.next; j++) {
			this.values[j] = this.values[j + 1];
		}
		this.values[this.next - 1] = null;
		this.next--;
	}
	
	//return the current size
	public int size() {
		return this.next;
	}
	
	//get card using the given index
	public Card getCard(int i) {
		if (i >= next) {
			throw new
			ArrayIndexOutOfBoundsException("invalid input");
		}
	return this.values[i];
	}
	 
	public int getTopValue() {
		int topVal = -1;
		for (int i = 0; i < values.length; i++){
			if (values[i] == null) {
				break;
			}
			else if(values[i].getValue() == 0) {
				topVal = 1;
				break;
			}
			else if (values[i].getValue() > topVal){
				topVal = values[i].getValue();
			}
		}
		return topVal;
	}
	
	//Show all cards available
	public String showAllCards() {
		int count = 0;
		String hand = "";
		for(Card card : values){
			if(card == null){
				break;
			}
			count++;
			hand += count + ". " + card.printValue() + " " + card.getSuite() + "\n";
	}
		return hand;
	}

	public Card[] getCardArray(){
		return this.values;
	}

	public void set(int position, Card newCard) {
		this.values[position] = newCard;
	}

}
