import java.util.*;
public class Game {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Deck deck = new Deck();
        System.out.println("Input first player name: ");
        String p1Name = reader.nextLine();
        Player p1 = new Player(p1Name);
        System.out.println("Input second player name: ");
        String p2Name = reader.nextLine();
        Player p2 = new Player(p2Name);
        System.out.println("Shuffling deck...");
        deck.shuffle();
        System.out.println("Dealing cards...");
        for(int i = 0;i < 5; i++){
            p1.fillCard(deck.getCardFromTop());
            deck.removeTopCard();
            p2.fillCard(deck.getCardFromTop());
            deck.removeTopCard();
        }



        Combination combiP1 = new Combination(p1.getHand());
        Combination combiP2 = new Combination(p2.getHand());


        
        System.out.println(p1Name + "'s cards are: " + p1.toString());
        System.out.println(p1Name + " has a " + combiP1.getScoreString());

        System.out.println("You can remove maximum 3 cards");
        for(int j = 0; j < 3; j++){
            System.out.println("Write down a card position you want to be replaced, write down 0 if you dont want to change.");
            int replacePosition = reader.nextInt() - 1;
            if(replacePosition == -1){
                break;
            }
            p1.set(replacePosition, deck.getCardFromTop());
            deck.removeTopCard();

            combiP1 = new Combination(p1.getHand());
            System.out.println(p1Name + "'s new cards are: " + p1.toString());
            System.out.println(p1Name + " has a " + combiP1.getScoreString());
        }


        System.out.println(p2Name + " cards are: " + p2.toString());
        System.out.println(p2Name + " has a " + combiP2.getScoreString());

        
        System.out.println("You can remove maximum 3 cards");
        for(int k = 0; k < 3; k++){
            System.out.println("Write down a card position you want to be replaced, write down 0 if you dont want to change.");
            int replacePosition = reader.nextInt() - 1;
            if(replacePosition == -1){
                break;
            }
            p2.set(replacePosition, deck.getCardFromTop());
            deck.removeTopCard();
            combiP2 = new Combination(p2.getHand());
            System.out.println(p2Name + "'s new cards are: " + p2.toString());
            System.out.println(p2Name + " has a " + combiP2.getScoreString());
        }
        if(combiP2.getScoreNum() > combiP1.getScoreNum()){
            System.out.println("Player " + p2Name + " wins with " + combiP2.getScoreString());
        }
        else if (combiP2.getScoreNum() < combiP1.getScoreNum()){
            System.out.println("Player " + p1Name + " wins with " + combiP1.getScoreString());
        }
        else{
            if(p1.getTopValue() > p2.getTopValue()){
                System.out.println("Player " + p1Name + " wins with " + combiP1.getScoreString() + " of "+ p1.getTopValue());
            }
            else if(p1.getTopValue() < p2.getTopValue()){
                System.out.println("Player " + p2Name + " wins with " + combiP2.getScoreString() + " of "+ p2.getTopValue());
            }
            else {
                System.out.println("It is a draw!");
            }
        }
    }
}
