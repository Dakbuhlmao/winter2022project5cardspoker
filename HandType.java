public enum HandType {
  ROYAL_FLUSH(10, "Royal Flush"),
  STRAIGHT_FLUSH(9, "Straight Flush"),
  FOUR_OF_A_KIND(8, "Four of A Kind"),
  FULL_HOUSE(7, "Full House"),
  FLUSH(6, "Flush"),
  STRAIGHT(5, "Straight"),
  THREE_OF_A_KIND(4, "Three of A Kind"),
  TWO_PAIR(3, "Two Pair"),
  PAIR(2, "Pair"),
  HIGH_CARD(1, "High Card");

  private  int handTypeNum;
  private  String handType;

  private HandType(final int v, final String vn) {
      handTypeNum = v;
      handType = vn;
  }

  public int getVal() {
      return handTypeNum;
  }
  public String getValString() {
    return handType;
  }
}