public class Player {
  private String name;
  private DynamicCardArray hand;
 
  public Player(String name) {
    this.name = name;
    this.hand = new DynamicCardArray();
  }
  public String getName() {
    return name;
  }
  public DynamicCardArray getHand() {
    return hand;
  }
  public String toString() {
    return hand.showAllCards();
  }
  public void fillCard(Card s){
    this.hand.addCardToBottom(s);
  }
  // debugging method
  public void printHand(){
    for(Card c: hand.getCardArray()){
      if (c == null) break;
      System.out.println(c);
    }
  }
  public void set(int position, Card newCard){
    hand.set(position, newCard);
  }
  public void seth(DynamicCardArray h){
    hand = h;
  }
  public int getTopValue(){
    return hand.getTopValue();
  }
}