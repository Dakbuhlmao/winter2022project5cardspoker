public enum Suite {
	HEART("Heart", 2),
	DIAMOND("Diamond", 1),
	CLUB("Club", 3),
	SPADE("Spade", 0);
	private final String suites;
	private final int numSuites;
	//Why do we need the "final"
	private Suite(final String suites, final int numSuites) {
		this.suites = suites;
		this.numSuites = numSuites;
	}	
	
	public String getSuite() {
		return this.suites;
	}

	public int getNumSuite() {
		return this.numSuites;
	}
}