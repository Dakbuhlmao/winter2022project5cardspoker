public enum Value {
	ACE(0, "Ace"),
	TWO(1, "2"),
	THREE(2, "3"),
	FOUR(3, "4"),
	FIVE(4, "5"),
	SIX(5, "6"),
	SEVEN(6, "7"),
	EIGHT(7, "8"),
	NINE(8, "9"),
	TEN(9, "10"),
	JACK(10, "Jack"),
	QUEEN(11, "Queen"),
	KING(12, "King");
	
	private final int values;
	private final String stringValues;
	
	private Value(final int values, final String stringValues) {
		this.values = values;
		this.stringValues = stringValues;
	}	
	public int getValue() {
		return this.values;
	}
	
	public String printValue() {
		return this.stringValues;
	}
	
}